import React from 'react';
import './App.css';
import Menu from './Menu';
import DateTime from './DateTime';
import Products from './Products';
import Contact from './Contact';
import './Menustyle.css';
import './DateTime.css';
import './Contact.css';
import './Products.css';


import {BrowserRouter, Route, Switch} from 'react-router-dom';

function App() {
  return (
    
    <BrowserRouter>
    <header>
    </header>
    <div >
    <div className="container">
    <Menu />
    <Switch>
    
          
    <Route path="/" exact component={DateTime} /> 
  
    <Route path="/Contact" component={Contact} /> 
    <Route path="/products" component={Products} /> 
    </Switch>
    </div>
    </div>
   
        
      
    
    </BrowserRouter>
  );
}

export default App;
