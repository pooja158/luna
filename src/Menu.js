import React from 'react';
import {Link} from 'react-router-dom';

const Menu = () => {
    return(
        <div className="Menustyle" id="box">
            <ul>
                <li><Link to="/" id="link">1.Choose Date&Time</Link></li>
                <li><Link to="Contact" id="link" >2.Contact Us</Link></li>
                <li><Link to="Products" id="link">3.Product/Rooms of interest</Link></li>
            </ul>
        </div>
    )
}
 export default Menu;   
