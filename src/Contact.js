import React, { Component } from 'react';

class Contact extends Component{
  constructor(props) {
    super(props)
    
    this.state = {
      firstname:'',
      lastname:'',
      phonenumber:'',
      email:" ",
      PromotionalCode:'',
      Address:'',
      city:'',
      zipcode:'',
      state:'',
      comments:''

   
    }
  }
  handlefirstnameChange = (event) => {
    this.setState({
      firstname: event.target.value
    })
  }

  handlelastnameChange = (event) => {
    this.setState({
      lastname: event.target.value
    })
  }
  handlephonenumberChange = (event) => {
    this.setState({
      phonenumber: event.target.value
    })
  }
  handleemailChange = (event) => {
    this.setState({
      email: event.target.value
    })
  }
  handlePromotionalCodeChange = (event) => {
    this.setState({
      PromotionalCode: event.target.value
    })
  }
  handleAddressChange = (event) => {
    this.setState({
     Address: event.target.value
    })
  }
  handlecityChange = (event) => {
    this.setState({
      city: event.target.value
    })
  }
  handlezipcodeChange = (event) => {
    this.setState({
      zipcode: event.target.value
    })
  }
  handlestateChange = (event) => {
    this.setState({
      state: event.target.value
    })
  }
  handlecommentsChange = (event) => {
    this.setState({
      comments: event.target.value
    })
  }
  handleSubmit = (event) =>{
    alert('${this.state.firstname} ${this.state.lastname}  ${this.state.phonenumber}  ${this.state.email}  ${this.state.PromotionalCode}  ${this.state.Address}  ${this.state.city}  ${this.state.zipcode}  ${this.state.state}  ${this.state.comments} ');
   
  
    event.preventDefault()

  }
 
  render() {
    return (
      <div className="Contact" id="box">
          <h1>Tell us About Yourself</h1>
          <form onSubmit={this.handleSubmit}>
          <div className="block1">
      
  <label for="firstname">
    <span>First Name*</span>
    <input type="text" value={this.state.firstname} onChange={this.handlefirstnameChange} id="Username" />
    
  </label>
  <label for="Last Name">
    <span>Last Name *</span>
    <input type="text" value={this.state.lastname} onChange={this.handlelastnameChange} id="Last Name" />
  </label>

  <label for="Phone Number">
    <span>Phone Number *</span>
    <input type="number" value={this.state.phonenumber} onChange={this.handlephonenumberChange} id="number" />
  </label>


  <label for="Email">
    <span>Email *</span>
    <input type="email" value={this.state.email} onChange={this.handleemailChange}  id="email" />
  </label>
  <label for="Promotional Code">
    <span>Promotional Code *</span>
    <input type="number" value={this.state.PromotionalCode} onChange={this.handlePromotionalCodeChange} id="number" />
  </label>
 
  
  <div id="house-built">
    <span>Was your house built before 1978?</span>
    <select value={this.state.PromotionalCode} onChange={this.handlePromotionalCodeChange} defaultValue="Unknown" id="code">
      <option defaultValue>Unknown</option>
      <option value="yes">yes</option>
      <option value="no">no</option>
    </select>
  </div>
 
</div>


<div className="block2">

<label for="Address">
    <span>Address*</span>
    <input type="text" value={this.state.Address} onChange={this.handleAddressChange} id="Last Name" />
    <input type="text" value={this.state.Address} onChange={this.handleAddressChange} id="Last Name" />
  </label>

  <label for="Last Name">
    <span>City*</span>
    <input type="text" value={this.state.city} onChange={this.handlecityChange} id="Last Name" />
  </label>


    <label for="State">
    <span>state*</span>
    <input type="text" value={this.state.state} onChange={this.handlestateChange} id="Last Name" />
  </label>
  <label for="Zip">
    <span>Zip *</span>
    <input type="number" value={this.state.zipcode} onChange={this.handlezipcodeChange} id="number" />
  </label>

  <label for="message">
    <span>Additional comments</span>
    <textarea type="text" value={this.state.comments} onChange={this.handlecommentsChange} id="message" />
  </label>


  <div id="preferred-contact">
    <span>Preferred Contact Method*</span>
    <select id="code">
      <option>Email</option>
      <option>Message</option>
      <option>contact no.</option>
    </select>
    </div>

   
</div>

<button id="send" type="submit">send</button>
 </form> 

    
  
  
     </div>
    ) 
  }
    
}
export default Contact;
